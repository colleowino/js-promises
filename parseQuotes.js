'use strict';

const axios = require('axios');
const cheerio = require('cheerio');

function extractQuote(node) {
  let quotes = [];
  let authorLinks = [];
  const $ = cheerio.load(node);

  $('div.quote').each(function (i, e) {
    let text = $(this).children('.text').text().trim().slice(0, 50);
    let author = $(this).find('.author').text().trim();
    let authorLink = $(this).find('.author').next().attr('href');
    authorLinks.push(authorLink);
    let tags = [];
    $(this)
      .find('a.tag')
      .each((i, e) => {
        tags[i] = e.firstChild.data.trim();
      });
    quotes.push({ text, author, tags });
  });

  return { quotes, authorLinks };
}

const fetchQuotes = async (page = 1) => {
  let data = [];
  await axios
    .get(`http://quotes.toscrape.com/page/${page}`)
    .then(function (response) {
      data = extractQuote(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
  return data;
};

function extractAuthor(node) {
  const $ = cheerio.load(node);

  let authorName = $('.author-title').text().trim();
  let biography = $('.author-description').text().trim().slice(0, 50);

  return { name: authorName, biography };
}

const fetchAuthor = async (author) => {
  let data = [];
  await axios
    .get(`http://quotes.toscrape.com/${author}`)
    .then(function (response) {
      data = extractAuthor(response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
  return data;
};

const fetchList = async (endPoint, params, keys) => {
  let apiCalls = [];
  let result = [];

  if (params == null) {
    params = [];
    for (let i = 1; i <= 10; i++) {
      params.push(i);
    }
  }

  for (let link of params) {
    apiCalls.push(endPoint(link));
  }

  await Promise.all(apiCalls)
    .then((res) => {
      for (let item of res) {
        let found = item[keys] || item;
        result = result.concat(found);
      }
    })
    .catch((err) => console.log(err));

  return result;
};

const fetchAuthorLinks = async () => {
  let allAuthors = await fetchList(fetchQuotes, null, 'authorLinks');

  allAuthors = new Set(allAuthors);

  return { allAuthors };
};

const fetchAllQuotes = async (tag, author) => {
  let allQuotes = await fetchList(fetchQuotes, null, 'quotes');

  if (tag) {
    allQuotes = allQuotes.filter((x) => x.tags.includes(tag));
  }

  if (author) {
    allQuotes = allQuotes.filter((x) => x.author == author);
  }

  return { allQuotes };
};

const fetchAllBios = async (author) => {
  let data = await fetchAuthorLinks(author);
  let params = [...data.allAuthors];

  let authorBios = await fetchList(fetchAuthor, params);

  if (author) {
    authorBios = authorBios.filter((x) => x.name == author);
  }
  return { authorBios };
};

module.exports = {
  fetchQuotes,
  fetchAllQuotes,
  fetchAuthorLinks,
  fetchAuthor,
  fetchAllBios,
};
