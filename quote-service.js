const jsdom = require('jsdom');

const express = require('express');
const parser = require('./parseQuotes');

const createService = () => {
  const app = express();

  app.get('/quotes', async function (req, res) {
    console.log(req.query);
    let { tag, author } = req.query;
    let data = await parser.fetchAllQuotes(tag, author);
    console.log(data.allQuotes);
    res.send({ data: data.allQuotes });
  });

  app.get('/authors', async function (req, res) {
    let { name } = req.query;
    let data = await parser.fetchAllBios(name);
    res.send({ data: data.authorBios });
  });

  return app;
};

module.exports = { createService };
