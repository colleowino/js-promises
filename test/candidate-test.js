const chai = require('chai');
const chaiHttp = require('chai-http');
const { createService } = require('../quote-service');
const { expect } = chai;
chai.use(chaiHttp);
``;
const app = createService();

describe('Candidate Tests', () => {
  it('should return an array of quotes tagged "obvious"', async () => {
    /*
     This file is for your testing purposes and
     will not be part of your final submission.
     */

    const req = '/quotes';
    const res = await chai.request(app).get(req);
    // .query({ name: 'C.S. Lewis' });
    // console.log(res.body.data, 'res');
    // const expected = {
    //   data: [
    //     {
    //       author: 'Steve Martin',
    //       text: '“A day without sunshine is like, you know, night.”',
    //       tags: ['humor', 'obvious', 'simile'],
    //     },
    //   ],
    // };

    expect(res.status).to.equal(200);
    expect(res).to.be.json;
    expect(res.body, 'response.body').to.be.a('object');
    expect(res.body.data, 'response.body.data').to.be.a('array');
    // expect(res.body.data.length, 'response.body.data.length').to.equal(2);

    // if (!validateResponse(res.body.data, expected.data)) {
    //   expect.fail(null, null, getFailString(res.body, expected));
    // }
  });
});
