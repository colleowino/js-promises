'use strict';
const fetchAllQuotes = async () => {
  let allQuotes = [];
  let allAuthorLinks = [];

  for (let i = 1; i <= 2; i++) {
    let data = await parser.fetchQuotes();
    allQuotes = allQuotes.concat(data.quotes);
    allAuthorLinks = allAuthorLinks.concat(data.authorLinks);
  }

  return { allQuotes, allAuthorLinks };
};

module.exports = { fetchAllQuotes };
